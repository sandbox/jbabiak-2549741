<?php

/**
 * @file
 * Rules callbacks for the rules_watchdog_action module.
 */

/**
 * Implements hook_rules_action_info().
 */
function rules_watchdog_action_rules_action_info() {
  $actions = array();

  // Add a log entry.
  $actions['rules_watchdog_action_log'] = array(
    'label' => t('Log a message to system log'),
    'parameter' => array(
      'message' => array(
        'type' => 'text',
        'label' => t('Log message'),
      ),
      'severity' => array(
        'type' => 'token',
        'label' => t('Severity'),
        'description' => t('An <a href="@rfc3164">RFC 3164</a> message severity.', array(
          '@rfc3164' => 'https://tools.ietf.org/html/rfc3164',
        )),
        'options list' => '_rules_watchdog_action_list_severity',
        'restriction' => 'input',
        'default value' => WATCHDOG_NOTICE,
        'optional' => TRUE,
      ),
      'link' => array(
        'type' => 'uri',
        'label' => t('Link'),
        'description' => t('A Drupal path, path alias, or external URL to associate with the message.'),
        'optional' => TRUE,
      ),
    ),
    'group' => t('Logging'),
  );

  return $actions;
}

/**
 * Rules action callback: Add a log entry.
 *
 * @param string $message
 *   The message to log.
 * @param int $severity
 *   The RFC 3164 severity of the log message.
 * @param string|null $link
 *   A Drupal path, path alias, or external URL to associate with the message.
 *
 * @see logging_severity_levels
 * @see watchdog()
 * @see watchdog_severity_levels()
 */
function rules_watchdog_action_log($message, $severity = WATCHDOG_NOTICE, $link = NULL) {
  // The contents of $message will already contain token-replaced variables, and
  // will already be translated, so we insert it into the log message as a
  // variable.
  watchdog('rules_watchdog_action', '@rules_watchdog_action_message', array(
    '@rules_watchdog_action_message' => (string) $message,
  ), (int) $severity, $link);
}
